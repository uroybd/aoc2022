// Advent of Code 2022 - Day 17

use std::{
    collections::{HashMap, HashSet},
    fs,
};

type Position = (isize, isize);

#[derive(Debug)]
struct Rock {
    shape: Vec<Position>,
    position: Position,
}

enum Movement {
    LEFT,
    RIGHT,
    DOWN,
}

impl Movement {
    fn value(&self) -> Position {
        match self {
            Self::LEFT => (-1, 0),
            Self::RIGHT => (1, 0),
            Self::DOWN => (0, -1),
        }
    }
}

fn parse_movements(file_path: String) -> Vec<Movement> {
    fs::read_to_string(file_path)
        .unwrap()
        .trim()
        .chars()
        .map(|c| match c {
            '>' => Movement::RIGHT,
            '<' => Movement::LEFT,
            _ => unreachable!(),
        })
        .collect()
}

impl Rock {
    fn get_shape_position(&self, pos: Position) -> Vec<(isize, isize)> {
        self.shape
            .iter()
            .map(|(x, y)| (x + pos.0, y + pos.1))
            .collect()
    }
    fn new(variant: usize, position: Position) -> Self {
        match variant {
            0 => Self {
                shape: vec![(0, 0), (1, 0), (2, 0), (3, 0)],
                position,
            },
            1 => Self {
                shape: vec![(1, 0), (0, -1), (1, -1), (2, -1), (1, -2)],
                position: (position.0, position.1 + 2),
            },
            2 => Self {
                shape: vec![(2, 0), (2, -1), (0, -2), (1, -2), (2, -2)],
                position: (position.0, position.1 + 2),
            },
            3 => Self {
                shape: vec![(0, 0), (0, -1), (0, -2), (0, -3)],
                position: (position.0, position.1 + 3),
            },
            4 => Self {
                shape: vec![(0, 0), (1, 0), (0, -1), (1, -1)],
                position: (position.0, position.1 + 1),
            },
            _ => unreachable!(),
        }
    }

    fn move_to(&mut self, room: &HashSet<Position>, direction: &Movement) -> Option<Vec<Position>> {
        let displacement = direction.value();
        let new_pos = (
            self.position.0 + displacement.0,
            self.position.1 + displacement.1,
        );

        let new_positions = self.get_shape_position(new_pos);
        if !new_positions
            .iter()
            .any(|p| p.0 < 0 || p.0 > 6 || p.1 < 0 || room.contains(p))
        {
            self.position = new_pos;
            return Some(new_positions);
        }

        None
    }
}

pub fn solution_day_17_01(file_path: String, count: usize) -> Option<isize> {
    let movements = parse_movements(file_path);
    let m_length = movements.len();
    let mut room = HashSet::new();
    let mut height = 0;
    let mut m_index = 0;
    for i in 0..count {
        let mut rock = Rock::new(i % 5, (2, height + 3));
        loop {
            let m = &movements[m_index % m_length];
            rock.move_to(&room, m);
            m_index += 1;
            let movement_result = rock.move_to(&room, &Movement::DOWN);
            if movement_result.is_none() {
                room.extend(rock.get_shape_position(rock.position));
                height = room.iter().map(|(_, y)| *y).max().unwrap() + 1;
                break;
            }
        }
    }

    Some(height)
}

#[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Debug)]
struct StateKey {
    gust_index: usize,
    rock_type: usize,
    height_profile: [isize; 7],
}

#[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Debug)]
struct StateValue {
    rock_index: usize,
    well_height: isize,
}

fn height_profile(room: &HashSet<(isize, isize)>) -> [isize; 7] {
    let mut profile = [0; 7];
    for i in 0..7isize {
        profile[i as usize] = room
            .iter()
            .filter(|(x, _)| x == &i)
            .map(|(_, y)| *y)
            .max()
            .unwrap_or(0);
    }
    let lowest = profile.into_iter().min().unwrap();
    for i in 0..7 {
        profile[i] -= lowest;
    }
    profile
}

pub fn solution_day_17_02(file_path: String, count: usize) -> Option<isize> {
    let movements = parse_movements(file_path);
    let m_length = movements.len();
    let mut room = HashSet::new();
    let mut height = 0;
    let mut m_index = 0;
    let mut bonus = 0;
    let mut cache: HashMap<StateKey, StateValue> = HashMap::new();
    let mut i = 0;
    let mut cycled = false;
    while i < count {
        let mut rock = Rock::new(i % 5, (2, height + 3));
        loop {
            let m = &movements[m_index % m_length];
            rock.move_to(&room, m);
            m_index += 1;
            let movement_result = rock.move_to(&room, &Movement::DOWN);
            if movement_result.is_none() {
                i += 1;
                room.extend(rock.get_shape_position(rock.position));
                height = room.iter().map(|(_, y)| *y).max().unwrap() + 1;
                let state_key = StateKey {
                    gust_index: m_index % m_length,
                    rock_type: i % 5,
                    height_profile: height_profile(&room),
                };
                let state_value = StateValue {
                    rock_index: i,
                    well_height: height,
                };
                if !cycled {
                    if let Some(&last_value) = cache.get(&state_key) {
                        let height_gain = height - last_value.well_height;
                        let rocks_in_cycle = i - last_value.rock_index;
                        let skipped = (count - i) / rocks_in_cycle;

                        i += skipped * rocks_in_cycle;
                        bonus = (skipped as isize) * height_gain;

                        cycled = true;
                    } else {
                        cache.insert(state_key, state_value);
                    }
                }

                break;
            }
        }
    }
    Some(height + bonus)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day_17_01() {
        let file_path: String = String::from("src/inputs/day17e.txt");
        let result = solution_day_17_01(file_path, 2022).unwrap();
        assert_eq!(result, 3068);
    }

    #[test]
    fn test_day_17_02() {
        let file_path: String = String::from("src/inputs/day17e.txt");
        let result = solution_day_17_02(file_path, 1_000_000_000_000).unwrap();
        assert_eq!(result, 1514285714288);
    }

    #[test]
    #[ignore]
    fn output_day_17_01() {
        let file_path: String = String::from("src/inputs/day17.txt");
        let result = solution_day_17_01(file_path, 2022);
        dbg!(result.unwrap());
        assert_eq!(1, 1);
    }

    #[test]
    #[ignore]
    fn output_day_17_02() {
        let file_path: String = String::from("src/inputs/day17.txt");
        let result = solution_day_17_02(file_path, 1_000_000_000_000);
        dbg!(result.unwrap());
        assert_eq!(1, 1);
    }
}
